public class Test {

    private VersionCache versionCache = VersionCacheImpl.getInstance();

    public void testCache() {
        getConfigThread().start();
        updateVersion().start();
        updateVersion().start();
        updateVersion().start();
        updateVersion().start();
        updateVersion().start();
        updateVersion().start();
        updateVersion().start();
        updateVersion().start();
        updateVersion().start();
        updateVersion().start();

    }


    private Thread getConfigThread() {
        return new Thread(() -> {
            for (int i = 0; i < 1000; i++) {
                System.out.println("getConfig: " + versionCache.get().getConf().toString());
                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private Thread updateVersion() {
        return new Thread(() -> System.out.println("updateVersion" + versionCache.promoteVersion()));
    }
}
