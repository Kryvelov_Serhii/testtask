
import java.util.Map;

public class Config {

    private Map<String, String> conf;

    public Config() {
    }

    public Map<String, String> getConf() {
        return conf;
    }

    public void setConf(Map<String, String> conf) {
        this.conf = conf;
    }
}
