import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;

public class VersionCacheImpl implements VersionCache {

    private static VersionCache versionCache;

    private final Boolean monitor = true;

    private AtomicInteger currentVersion = new AtomicInteger(0);
    private Config config;

    public static synchronized VersionCache getInstance() {
        if (versionCache == null) {
            versionCache = new VersionCacheImpl();
        }
        return versionCache;
    }

    private VersionCacheImpl() {
        initial();
    }


    @Override
    public synchronized Config get() {
        return config;
    }

    @Override
    public int promoteVersion() {
        int actualVersion = currentVersion.incrementAndGet();
        System.out.println("promote to " + actualVersion);
        Optional<Config> actualConfig = updateCache(actualVersion);
        actualConfig.ifPresent(this::reloadCache);
        return actualVersion;
    }

    private void initial() {
        if (config != null)
            return;
        Optional<Config> actualConfig = updateCache(0);
        actualConfig.ifPresent(this::reloadCache);
    }

    private Optional<Config> updateCache(int actualVersion) {
        synchronized (monitor) {
            // upload cache
            try {
                if (currentVersion.get() > actualVersion) {
                    System.out.println("failed to start update to version " + actualVersion + "." + "newer version available " + currentVersion.get());
                    return Optional.empty();
                } else {
                    System.out.println("starting update to version "  + actualVersion);
                }
                /*start uploading data simulation*/
                monitor.wait(5000);
                Map<String, String> configData = new HashMap<>();
                configData.put(currentVersion.toString(), currentVersion.toString());
                Config actualConfig = new Config();
                actualConfig.setConf(configData);
                /*end uploading data simulation*/
                if (currentVersion.get() > actualVersion) {
                    System.out.println("update to " + actualVersion + " rejected." + "newer version available " + currentVersion.get());
                    return Optional.empty();
                }
                System.out.println("update to " + actualVersion + " accepted.");
                return Optional.of(actualConfig);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return Optional.empty();
        }
    }

    private synchronized void reloadCache(Config actualConfig) {
        config = actualConfig;
    }
}
