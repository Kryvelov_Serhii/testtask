public interface VersionCache {
    /**
     * @return cached instance model
     */
    Config get();

    /**
     * Increment the required version. This hints cache loading mechanism to update it's cache
     */
    int promoteVersion();
}