Description:
Our application configuration object is pretty big is stored in the DB. To improve performance, we need to implement a cache which can be called by multiple threads to get the configuration object, i.e.  cache,get() returns Config object.
When there is a config change, this cache will be hinted that a version is available in the DB (some thread will call (cache.promoteVersion()), and when new thread/s call cache.get(), the caching mechanism will refresh the cache.

Task:
Implement a cache that that return the Config object on demand (when calling cache.get()). If there is no cached object yet, populate the cache on demand synchronously, if there is already cached Config object but newer version is available (someone called promoteVersion()), asynchronously update the cache and during the cache update duration, calling cache.get() should serve the old cached version.

Notes:
You only need to implement the cache handling code, there is no need to implement a code that actually fetch a Config object from the DB, this can be abstract.